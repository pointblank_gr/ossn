# OSSN

Welcome to Open Source Student Network page repository!


#### Contents
1. [Set up](#setup)
2. [Develop](#develop)
3. [Build](#build)


### <a name="setup"></a>Setting up the project
1. Install `nodeJS`.Use your OS package manager to install nodeJS or follow the instructions on [nodeJS page](https://nodejs.org/en/download/).
2. install `gatsby-cli`. Run
 `npm install --global gatsby-cli`.
2. Clone the repository. Run `git clone http://link.git`.
3. Install node dependencies. On repo directory run `npm install`.

### <a name="develop"></a> Develop
1. Open development server.
At the repository directory run `gatsby develop`
- The page will be available at [localhost:8000](htpp://localhost:8000) by default

### <a name="build"></a> Build

1. Building the page. At the repository directory run `gatsby build`
2. Start the server. Run `gatsby build` to initialize a gatsby server at your machine.
- The page will be available  [localhost:9000](http://localhost:9000) by default.


Find out more about the tools we used at [https://www.gatsbyjs.org/](https://www.gatsbyjs.org/)
